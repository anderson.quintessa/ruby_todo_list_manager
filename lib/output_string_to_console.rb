require_relative 'input_string_from_console'

class OutputStringToConsole

  def welcome_user
    puts "\nWelcome to the Ruby Command Line Todo List App\n\n" "You have 0 items on your list\n\n"
  end

  def prompt_user_to_add_item
    puts "\nWould you like to add a new item? Please type: [Yes/No]\n"
  end

  def set_exit_mode(user_choice)
    if user_choice == "no" then
      prompt_user_to_return_later
    elsif
      prompt_user_to_type_item
    end
  end

  def prompt_user_to_type_item
    puts "\nPlease type a new item to add to your list\n"
  end

  def prompt_user_to_return_later
    puts "\nPlease come back when you are ready to add an item to the list\n"
  end

  def output_to_show_list(list)
    puts "\nHere is your current todo list: \n"
    puts "-" << list
  end
end
