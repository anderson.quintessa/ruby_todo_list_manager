require_relative 'messages_to_console'

class InputToConsole

  def get_user_answer
    gets.chomp.downcase
  end
end
