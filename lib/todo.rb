require_relative 'input_to_console'
require_relative 'messages_to_console'
require_relative 'messages'
require_relative 'todo_list'
require_relative 'todo_runner'

todo = ToDoRunner.new(InputToConsole.new, Messages.new, MessagesToConsole.new, ToDoList.new)

todo.run
