require_relative 'input_to_console'

class Messages

  def welcome_user(todo_item_count)
    "\nWelcome to the Ruby Command Line Todo List App\n\n" "You have " + todo_item_count + " items on your list\n\n"
  end

  def show_total_item_count(todo_item_count)
    "\nYou have " + todo_item_count + " items on your list\n\n"
  end

  def prompt_user_to_add_item
    "\nWould you like to add a new item? Please type: [Yes/No]\n"
  end

  def prompt_user_to_type_item
    "\nPlease type a new item to add to your list\n"
  end

  def prompt_user_to_return_later
    "\nPlease come back when you are ready to add an item to the list\n"
  end

  def current_list(formatted_list)
    "\nHere is your current todo list: \n\n#{formatted_list}\n"
  end

  def get_yes_or_no_answer(user_choice)
    if user_choice == "no" then
      prompt_user_to_return_later
    elsif user_choice == "yes"
      prompt_user_to_type_item
    end
  end
end
