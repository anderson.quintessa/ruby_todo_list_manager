require 'pry'
class ToDoList

  attr_accessor :items

  def initialize
    @items = []
  end

  def add_item(new_item)
    puts "New Item on list: #{new_item}"
    if new_item != ""
      items << new_item
    end
  end

  def stringify_list
    items.map.with_index{|item, index| "#{index+=1}. #{item}"}
    .join("\n")
  end
end
