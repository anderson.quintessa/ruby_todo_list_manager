require_relative 'input_to_console'
require_relative 'messages'
require_relative 'messages_to_console'
require_relative 'todo_list'

class ToDoRunner
  attr_accessor :user_choice, :input_to_console, :messages_to_console
  attr_reader :messages, :todo_list

  def initialize(input_to_console, messages, messages_to_console, todo_list)
    @input_to_console = input_to_console
    @messages = messages
    @messages_to_console = messages_to_console
    @todo_list = todo_list
  end

  def welcome
    total_item_count = todo_list.items.count.to_s
    messages_to_console.print_message(messages.welcome_user(total_item_count))
  end

  def get_user_choice
    messages_to_console.print_message(messages.prompt_user_to_add_item)
    @user_choice = input_to_console.get_user_answer
    messages_to_console.print_message(messages.get_yes_or_no_answer(user_choice))
  end

  def add_new_item
    new_item = input_to_console.get_user_answer
    todo_list.add_item(new_item)
  end

  def display_current_list(updated_list)
    messages_to_console.print_message(messages.current_list(updated_list))
  end

  def display_item_count(total_item_count)
    messages_to_console.print_message(messages.show_total_item_count(total_item_count))
  end

  def run
    welcome
    loop do
      get_user_choice
      if user_choice != "yes"
        break
      end
      add_new_item
      updated_list = todo_list.stringify_list
      display_current_list(updated_list)
      total_item_count = todo_list.items.count.to_s
      display_item_count(total_item_count)
   end
 end
end
