require 'messages_to_console'
require 'messages'
require 'todo_list'

RSpec.describe MessagesToConsole do
  let(:messages) { Messages.new }
  let(:todo_list) { ToDoList.new }
  let(:messages_to_console) {
    obj = double()
    allow(obj).to receive(:print_message).with(messages.prompt_user_to_type_item)
    .and_return("Please type a new item to add to your list")
   }

  describe '#welcome_message' do
    it "returns a formatted string to greet the user" do
      todo_item_count = todo_list.items.count.to_s

      expect(messages.welcome_user(todo_item_count))
      .to eq("\nWelcome to the Ruby Command Line Todo List App\n\n" "You have " + todo_item_count + " items on your list\n\n")
   end
 end

 describe '#show_total_item_count' do
   it "returns a formatted string with the total number of items on the list" do
     todo_item_count = todo_list.items.count.to_s

     expect(messages.show_total_item_count(todo_item_count))
     .to eq("\nYou have " + todo_item_count + " items on your list\n\n")
   end
 end

describe '#current_list' do
  it "returns a formatted string with the list" do
    formatted_list = "Feed the dog"

    expect(messages.current_list(formatted_list))
    .to eq("\nHere is your current todo list: \n\n#{formatted_list}\n")
  end
end

  describe "#get_yes_or_no_answer" do
    it "exits the user out of the app" do
      user_choice = "no"

      expect(messages.get_yes_or_no_answer(user_choice))
      .to eq("\nPlease come back when you are ready to add an item to the list\n")
    end

    it "prompts the user to type an item" do
      user_choice = "yes"

      expect(messages.get_yes_or_no_answer(user_choice))
      .to eq("\nPlease type a new item to add to your list\n")
    end
  end
end
