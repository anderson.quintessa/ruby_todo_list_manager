require 'messages_to_console'
require 'input_to_console'
require 'todo_list'
require 'todo_runner'

RSpec.describe ToDoRunner do
  let(:todo_runner) { ToDoRunner.new(input_to_console, messages, messages_to_console, todo_list) }
  let(:input_to_console) { InputToConsole.new }
  let(:messages) { Messages.new }
  let(:messages_to_console) { MessagesToConsole.new }
  let(:todo_list) { ToDoList.new }

  describe "#welcome" do
    it "welcomes the user when the app is run" do
    todo_item_count = todo_list.items.count.to_s

    allow(messages_to_console)
    .to receive(:print_message)
    .and_return("\nWelcome to the Ruby Command Line Todo List App\n\n" "You have " + todo_item_count + " items on your list\n\n")

    expect(todo_runner.welcome).to eq("\nWelcome to the Ruby Command Line Todo List App\n\n" "You have " + todo_item_count + " items on your list\n\n")
    end
  end

  describe "#get_user_choice" do
    it "prompts the user to type an item if the users' choice is yes" do
      allow(input_to_console)
      .to receive(:gets)
      .and_return("yes")

      allow(messages_to_console)
      .to receive(:print_message)
      .and_return("\nPlease type a new item to add to your list\n")

      expect(todo_runner.get_user_choice).to eq("\nPlease type a new item to add to your list\n")
    end

    it "prompts the user to type an item if the users' choice is no" do
      allow(input_to_console)
      .to receive(:gets)
      .and_return("no")

      allow(messages_to_console)
      .to receive(:print_message)
      .and_return("\nPlease come back when you are ready to add an item to the list\n")

      expect(todo_runner.get_user_choice).to eq("\nPlease come back when you are ready to add an item to the list\n")
    end
  end

  describe "#add_new_item" do
     it "adds an item to the list" do
       allow(input_to_console)
       .to receive(:gets)
       .and_return("stub")

       expect(todo_runner.add_new_item).to eq(["stub"])
     end
   end

  describe "#display_current_list" do
    it "displays the current list as a formatted string" do
      allow(messages_to_console)
      .to receive(:print_message)
      .and_return("\nHere is your current todo list: \n\n1. stub\n")

      updated_list = todo_list.stringify_list

      expect(todo_runner.display_current_list(updated_list)).to eq("\nHere is your current todo list: \n\n1. stub\n")
    end
  end

  describe "#display_item_count" do
    it "displays the total item count" do
      todo_item_count = todo_list.items.count.to_s

      allow(messages_to_console)
      .to receive(:print_message)
      .and_return("\nYou have " + todo_item_count + " items on your list\n\n")

      expect(todo_runner.display_item_count(todo_item_count)).to eq("\nYou have " + todo_item_count + " items on your list\n\n")
    end
  end
end
