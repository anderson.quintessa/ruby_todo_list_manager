require 'todo_list'
require 'input_string_from_console'

RSpec.describe ToDoList do
  subject {ToDoList}

  describe '#add_item' do
    it "adds an item to the list" do
      list = ToDoList.new()
      item = "walk the dog"

      expect{list.add_item(item)}.to change{list.items.count}.from(0).to(1)
    end

    it "adds two items to the list" do
      list = ToDoList.new()
      item_1 = "walk the dog"
      item_2 = "do laundry"

      list.add_item(item_1)
      list.add_item(item_2)

      expect(list.items.count).to eq(2)
    end

    it "adds 8 items to the list" do
      list = ToDoList.new()
      item_1 = "walk the dog"
      item_2 = "do laundry"
      item_3 = "Go to Gym"
      item_4 = "Feed the dog"
      item_5 = "Grocery shopping"
      item_6 = "Deposit check"
      item_7 = "Buy milk"
      item_8 = "Read POODR"
      item_9 = "Clean up"
      item_10 = "Cook dinner"

      list.add_item(item_1)
      list.add_item(item_2)
      list.add_item(item_3)
      list.add_item(item_4)
      list.add_item(item_5)
      list.add_item(item_6)
      list.add_item(item_7)
      list.add_item(item_8)
      list.add_item(item_9)
      list.add_item(item_10)

      expect(list.items.count).to eq(10)
    end
  end

  describe '#stringify_list' do
      it "adds an item to the list as a formatted string" do
        item = "walk the dog"

        list.add_item(item)

        expect(list.stringify_list).to eq("1. walk the dog")
    end
  end

  describe '#list_in_numerical_order' do
    it "orders the items on the list in numerical order as a formatted string" do
      item_1 = "walk the dog"
      item_2 = "do laundry"
      item_3 = "Go grocery shopping"


      list.add_item(item_1)
      list.add_item(item_2)
      list.add_item(item_3)

      expect(list.stringify_list).to eq("1. walk the dog\n2. do laundry\n3. Go grocery shopping")
    end
  end
end
