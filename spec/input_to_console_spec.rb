require 'input_string_from_console'

RSpec.describe InputStringFromConsole do
  describe '#user_input_mode' do
    it "returns a formatted string received from input" do
      input_string_from_console = InputStringFromConsole.new

      allow(input_string_from_console)
      .to receive(:gets)
      .and_return("stUb\n")

      expect(input_string_from_console.get_user_answer).to eq("stub")
    end
   end

   describe '#get_new_todo_item' do
     it "returns a formatted string received from input" do
       input_string_from_console = InputStringFromConsole.new

       allow(input_string_from_console)
       .to receive(:gets)
       .and_return("Walk the Dog\n")

       expect(input_string_from_console.get_user_answer).to eq("walk the dog")
     end
    end
end
