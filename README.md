# CLI_Todo_List_Manager

**SUMMARY**

This is a command line interface application that allows the user to view their todo list

*Features Include*
* The ability to add multiple new items to the list

To run the application type: `ruby lib/todo.rb`

***

**To Add an Item**
type:
- `Walk the dog`
- `laundry`
- `go grocery shopping`

***

**FOR DEVELOPERS**

To run the test watcher type: `bundle exec guard`
See `.ruby_version` file for Ruby version

**GOALS**
1. Add multiple items to a list

---
___
